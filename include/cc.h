	#ifndef	_CC_H_
	#define	_CC_H_
	
	#include "variable.h"
	#include "comp.h"
//	#include "filter.h"

typedef struct _SPD_OBS
{
	float	Theta_est, Thetar_est, Err_Theta, Err_Theta_integ;
	float	AlphaKd, AlphaKp, AlphaKi, AlphaFF;
	float	Inv_Jm, Jm;
	float	Theta_est_d, Tl_est_d;
	float	Te_est_old, Te_est, Tl_est, Te_est_filter;
	float	Acc_integ;
	float	Ki_so, Kp_so, Kd_so, Wso, Fso;
	float	Wrm_est_d, Wr_est, Wrm_est, Wrpm_est;
	float	Wc_filter;
	float	alpha_ff;
	//IIR1	Filter_Te_set;
}  SpdObs;
extern SpdObs Obs1;
extern SpdObs Obs2;
void InitObs(SpdObs *tmpO, float Wc, float Jm);
extern void UpdateGainObs(Motor *tmpM, SpdObs *tmpO);
typedef struct _DIST_OBS
{
	float Fdis,Wdis;
	float bf_filt;
	float af_filt;
	float af_filt_prev;
	float DisTorque;
	float ratio;
	float senced;
	float Torque_ref;

} DstObs;
extern DstObs Obsd;




//extern void UpdateGainObs(SpdObs *tmpO);

void Switch1Off(void);
void Switch1On(void);


void ResetCC1(void);

void faultTz(void);
void ResetTZCC(void);
void PWMCarrierFreqSetting(int freq);
void SpeedObserver (Motor *tmpM, SpdObs *tmpO);

void DeadbandSetting(int DeadbandA, int DeadbandB, int DeadbandC);

float EncoderAngleGenerator(void);
void RotorAlign(void);


void Jestimate(Motor *tmpM, SpdObs *tmpO);
void PCProfileGen(void);
void SCProfileGen(void);
void PCPDcontroller(void);
void SCPIcontroller(void);
void CCPIcontroller(void);
void SVPWM(void);
void Deadtime_Compensation(void);
void PreLimiting(void);
void Deadtime_recover(void);
float SINRefGen(void);
extern void FaultProcess(void);
void LfindFunc(void);
float Thetar_added(float thetar);
void JfindSCRefCalib();
void InitDistObs(DstObs *tmpD, float Wc);
void UpdateGainObsd(DstObs *tmpD);
void DisturbObserver(Motor *tmpM, DstObs *tmpD);
#endif
