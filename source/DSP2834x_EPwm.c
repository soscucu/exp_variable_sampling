// TI File $Revision: /main/3 $
// Checkin $Date: August 1, 2008   16:35:35 $
//###########################################################################
//
// FILE:   DSP2834x_EPwm.c
//
// TITLE:  DSP2834x ePWM Initialization & Support Functions.
//
//###########################################################################
// $TI Release: DSP2834x C/C++ Header Files V1.10 $
// $Release Date: July 27, 2009 $
//###########################################################################

#include "DSP2834x_Device.h"     // DSP2834x Headerfile Include File
#include "DSP2834x_Examples.h"   // DSP2834x Examples Include File
#include "variable.h"

//---------------------------------------------------------------------------
// InitEPwm:
//---------------------------------------------------------------------------
// This function initializes the ePWM(s) to a known state.
//

#define EPWM1_DB   450		// 0x01C2
#define EPWM2_DB   450
#define EPWM3_DB   450
#define EPWM4_DB   450
#define EPWM5_DB   450
#define EPWM6_DB   450
#define EPWM7_DB   450
#define EPWM8_DB   450
#define EPWM9_DB   450




extern interrupt void cc(void);


void InitEpwm1(void)
{
//	half_Ts_cnt =  Ts_cnt/2;

//////////////////////////////////////////////////////////////////////////////////////////////////
	EALLOW;
	EPwm1Regs.TZCLR.bit.CBC = 1;
	EPwm1Regs.TZCLR.bit.INT = 1;
	EPwm1Regs.TZCLR.bit.OST = 1;

//110905 OSHT1 -> CBC2
//	EPwm1Regs.TZSEL.bit.OSHT1 = 1;
	EPwm1Regs.TZSEL.bit.CBC1 = 1;
	EPwm1Regs.TZCTL.bit.TZA = 2;	// Low state
	EPwm1Regs.TZCTL.bit.TZB = 2;	// Low state
//	EPwm1Regs.TZEINT.bit.OST = 1;
  EPwm1Regs.TZEINT.bit.CBC = 1;

	EDIS;

//	EPwm1Regs.TBPRD = PWM1_TIMER_TBPRD;				// Set timer period
//	EPwm1Regs.TBPHS.half.TBPHS = 0x0000;           	// Phase is 0
//	EPwm1Regs.TBCTR = 0x0000;                      	// Clear counter
//	EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; 	// Up-down-count mode
//	EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE;			// Disable phase loading. Mastser Module
//	EPwm1Regs.TBCTL.bit.PRDLD = TB_SHADOW;   // shadowing enabled
//	EPwm1Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO;
//	EPwm1Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;		// Clock ratio to SYSCLKOUT
//	EPwm1Regs.TBCTL.bit.CLKDIV = TB_DIV1;
//	EPwm1Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;    // Load registers every ZERO
//	EPwm1Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
//	EPwm1Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO_PRD;
//	EPwm1Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO_PRD;
	EPwm1Regs.TBPRD = uTsPerTdsp; // Period = 2��600 TBCLK counts
	EPwm1Regs.CMPA.half.CMPA = 0; //half_Ts; // Compare A = 400 TBCLK counts
//	EPwm1Regs.CMPB = 6000; // Compare B = 500 TBCLK counts
	EPwm1Regs.TBPHS.all = 0; // Set Phase register to zero
	EPwm1Regs.TBCTR = 0; // clear TB counter

	EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Symmetric
	EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE; // Phase loading disabled
	EPwm1Regs.TBCTL.bit.PRDLD = TB_SHADOW;
	EPwm1Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO;
	EPwm1Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1; // TBCLK = SYSCLKOUT
	EPwm1Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	EPwm1Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm1Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm1Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO_PRD; // Freeze
	EPwm1Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO_PRD; // Freeze

	EPwm1Regs.AQCTLA.bit.CAU = AQ_CLEAR;				//		init factor
//	EPwm1Regs.AQCTLA.bit.CAD = AQ_SET;					// 		flagrun factor
//	EPwm1Regs.AQCTLA.bit.CAD = AQ_CLEAR;

	


//	EPwm1Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;
//	EPwm1Regs.AQCTLA.bit.CAU = AQ_CLEAR;
//	EPwm1Regs.AQCTLA.bit.CAD = AQ_CLEAR;

//	EPwm1Regs.AQCTLB.bit.CBU = AQ_SET;
//	EPwm1Regs.AQCTLB.bit.CBD = AQ_CLEAR;
	// Setup compare
//	EPwm1Regs.CMPA.half.CMPA = HalfDuty;
//	EPwm1Regs.AQCTLA.bit.CAU = AQ_CLEAR;             // Set PWM1A on Zero
//	EPwm1Regs.AQCTLA.bit.CAD = AQ_SET;
	
	EPwm1Regs.AQSFRC.bit.OTSFA = 1;
	EPwm1Regs.AQSFRC.bit.OTSFB = 1;

		//Sungho ADD
	EPwm1Regs.AQSFRC.bit.RLDCSF = 3;
	EPwm1Regs.AQSFRC.bit.ACTSFA = 1; //Clear (LOW)
	EPwm1Regs.AQSFRC.bit.ACTSFB = 1; //Clear (LOW)

	EPwm1Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
	EPwm1Regs.DBCTL.bit.POLSEL =  DB_ACTV_HI; // active high complementary
	EPwm1Regs.DBCTL.bit.IN_MODE = DBA_ALL;

	

	EPwm1Regs.DBRED = EPWM1_DB;
	EPwm1Regs.DBFED = EPWM1_DB;



//	EPwm1Regs.ETSEL.bit.INTSEL = ET_CTRD_CMPA;     // Select INT on PRD event
	EPwm1Regs.ETSEL.bit.INTEN = 1;		// Enable INT
//	EPwm1Regs.ETSEL.bit.SOCAEN = 1;        // Enable SOC on A group
	EPwm1Regs.ETSEL.bit.INTSEL = ET_CTR_PRD;       // Select SOC from from CPMA on upcount

	EPwm1Regs.ETPS.bit.INTPRD = ET_1ST;           // Generate INT on 1st event
//	EPwm1Regs.ETPS.bit.SOCAPRD = ET_1ST;        // Generate pulse on 1st event

//////////////////////////////////////////////////////////////////////////////////////////////////
}


void InitEpwm2(void)
{

//////////////////////////////////////////////////////////////////////////////////////////////////
	EALLOW;
//	EPwm2Regs.TZCLR.bit.CBC = 1;
//	EPwm2Regs.TZCLR.bit.INT = 1;
//	EPwm2Regs.TZCLR.bit.OST = 1;

//110905 OSHT1 -> CBC2
//	EPwm2Regs.TZSEL.bit.OSHT1 = 1;
	EPwm2Regs.TZSEL.bit.CBC1 = 1;
	EPwm2Regs.TZCTL.bit.TZA = 2;	// Low state
	EPwm2Regs.TZCTL.bit.TZB = 2;	// Low state
//	EPwm2Regs.TZEINT.bit.OST = 1;
	EPwm2Regs.TZEINT.bit.CBC = 1;

	EDIS;
	
/*
	EPwm2Regs.TBPRD = uTsPerTdsp; // Period = 2��600 TBCLK counts
	EPwm2Regs.CMPA.half.CMPA = 0; // Compare A = 400 TBCLK counts
	
	//EPwm2Regs.TBPHS.all = 0; // Set Phase register to zero
	EPwm2Regs.TBPHS.half.TBPHS= 6249;  //(uTsPerTdsp/3*2)
//	EPwm2Regs.TBPHS.half.TBPHS= 0;  //(uTsPerTdsp/3*2)
		
	EPwm2Regs.TBCTR = 0; // clear TB counter
	

	EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Symmetric
  //EPwm2Regs.TBCTL.bit.PHSEN = TB_DISABLE; // Phase loading disabled
  
    EPwm2Regs.TBCTL.bit.PHSEN = TB_ENABLE; 
	EPwm2Regs.TBCTL.bit.PHSDIR = TB_DOWN; 

	EPwm2Regs.TBCTL.bit.PRDLD = TB_SHADOW;
    EPwm2Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;
  //EPwm2Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO;
	EPwm2Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1; // TBCLK = SYSCLKOUT
	EPwm2Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	EPwm2Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm2Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm2Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO_PRD; // Freeze
	EPwm2Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO_PRD; // Freeze

	EPwm2Regs.AQCTLA.bit.CAU = AQ_CLEAR;
//	EPwm2Regs.AQCTLA.bit.CAD = AQ_SET;

	EPwm2Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
	EPwm2Regs.DBCTL.bit.POLSEL =  DB_ACTV_HI; // active low complementary
	EPwm2Regs.DBCTL.bit.IN_MODE = DBA_ALL;
	EPwm2Regs.DBRED = EPWM5_DB;
	EPwm2Regs.DBFED = EPWM5_DB;

	EPwm2Regs.ETSEL.bit.INTSEL = ET_CTR_PRD;     // Select INT on Zero event
//	EPwm2Regs.ETSEL.bit.INTSEL = ET_CTRD_CMPB;
//	EPwm2Regs.ETSEL.bit.INTSEL = ET_CTRD_CMPA;

	EPwm2Regs.ETSEL.bit.INTEN = 1;		// Enable INT
	EPwm2Regs.ETPS.bit.INTPRD = ET_1ST; 

*/
	EPwm2Regs.TBPRD = uTsPerTdsp; // Period = 2��600 TBCLK counts (At triangle)
	EPwm2Regs.CMPA.half.CMPA =  0; // Compare A = 400 TBCLK counts
//	EPwm2Regs.CMPB = 3000; // Compare B = 500 TBCLK counts
	EPwm2Regs.TBPHS.all = 0; // Set Phase register to zero
	EPwm2Regs.TBCTR = 0; // clear TB counter

	EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Symmetric
	EPwm2Regs.TBCTL.bit.PHSEN = TB_DISABLE; // Phase loading disabled
	EPwm2Regs.TBCTL.bit.PRDLD = TB_SHADOW;
	EPwm2Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO;
	EPwm2Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1; // TBCLK = SYSCLKOUT
	EPwm2Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	EPwm2Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm2Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm2Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO_PRD; // Freeze
	EPwm2Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO_PRD; // Freeze

	EPwm2Regs.AQCTLA.bit.CAU = AQ_CLEAR;
//	EPwm2Regs.AQCTLA.bit.CAD = AQ_SET;
//	EPwm2Regs.AQCTLB.bit.CBU = AQ_CLEAR;
//	EPwm2Regs.AQCTLB.bit.CBD = AQ_SET;

	EPwm2Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
	EPwm2Regs.DBCTL.bit.POLSEL =  DB_ACTV_HI; // active low complementary

	EPwm2Regs.AQSFRC.bit.OTSFA = 1;
	EPwm2Regs.AQSFRC.bit.OTSFB = 1;
		//Sungho ADD
	EPwm2Regs.AQSFRC.bit.RLDCSF = 3;
	EPwm2Regs.AQSFRC.bit.ACTSFA = 1; //Clear (LOW)
	EPwm2Regs.AQSFRC.bit.ACTSFB = 1; //Clear (LOW)

	EPwm2Regs.DBCTL.bit.IN_MODE = DBA_ALL;
	EPwm2Regs.DBRED = EPWM2_DB;
	EPwm2Regs.DBFED = EPWM2_DB;

	EPwm2Regs.ETSEL.bit.INTSEL = ET_CTRU_CMPA;     // Select INT on Zero event
	EPwm2Regs.ETSEL.bit.INTEN = 1;		// Enable INT
//	EPwm2Regs.ETSEL.bit.INTEN = 0;		// Enable INT
//	EPwm2Regs.ETSEL.bit.SOCAEN = 1;        // Enable SOC on A group
//	EPwm2Regs.ETSEL.bit.SOCASEL = ET_CTR_ZERO;       // Select SOC from from CPMA on upcount
	EPwm2Regs.ETPS.bit.INTPRD = ET_1ST;           // Generate INT on 1st event
//	EPwm2Regs.ETPS.bit.SOCAPRD = ET_1ST;        // Generate pulse on 1st event


/////////////////////////////////////////////////////////////////////////////////////////////////
}

void InitEpwm3(void)
{

	EALLOW;
//	EPwm3Regs.TZCLR.bit.CBC = 1;
//	EPwm3Regs.TZCLR.bit.INT = 1;
//	EPwm3Regs.TZCLR.bit.OST = 1;

//110905 OSHT1 -> CBC2
//	EPwm3Regs.TZSEL.bit.OSHT1 = 1;
	EPwm3Regs.TZSEL.bit.CBC1 = 1;
	EPwm3Regs.TZCTL.bit.TZA = 2;	// Low state
	EPwm3Regs.TZCTL.bit.TZB = 2;	// Low state
//	EPwm3Regs.TZEINT.bit.OST = 1;
    EPwm3Regs.TZEINT.bit.CBC = 1;

	EDIS;
/*	
	EPwm3Regs.TBPRD = uTsPerTdsp; // Period = 2��600 TBCLK counts
	EPwm3Regs.CMPA.half.CMPA = 0; // Compare A = 400 TBCLK counts
//	EPwm3Regs.TBPHS.all = 0; // Set Phase register to zero
	EPwm3Regs.TBPHS.half.TBPHS= 6249;
//	EPwm3Regs.TBPHS.half.TBPHS= 9374;
	
	EPwm3Regs.TBCTR = 0; // clear TB counter

	EPwm3Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Symmetric
//	EPwm3Regs.TBCTL.bit.PHSEN = TB_DISABLE; // Phase loading disabled
	EPwm3Regs.TBCTL.bit.PHSEN = TB_ENABLE;
	EPwm3Regs.TBCTL.bit.PHSDIR = TB_UP; 
	
	EPwm3Regs.TBCTL.bit.PRDLD = TB_SHADOW;
//	EPwm3Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO;

	EPwm3Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;
	EPwm3Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1; // TBCLK = SYSCLKOUT
	EPwm3Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	EPwm3Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm3Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm3Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO_PRD; // Freeze
	EPwm3Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO_PRD; // Freeze6
	EPwm3Regs.AQCTLA.bit.CAU = AQ_CLEAR;
//	EPwm3Regs.AQCTLA.bit.CAD = AQ_SET;

	EPwm3Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
	EPwm3Regs.DBCTL.bit.POLSEL =  DB_ACTV_HI; // active low complementary
	EPwm3Regs.DBCTL.bit.IN_MODE = DBA_ALL;
	EPwm3Regs.DBRED = EPWM6_DB;
	EPwm3Regs.DBFED = EPWM6_DB;

	EPwm3Regs.ETSEL.bit.INTSEL = ET_CTR_PRD;     // Select INT on Zero event
//	EPwm3Regs.ETSEL.bit.INTSEL = ET_CTRD_CMPB;
//	EPwm3Regs.ETSEL.bit.INTSEL = ET_CTRD_CMPA;
	EPwm3Regs.ETSEL.bit.INTEN = 1;		// Enable INT
	EPwm3Regs.ETPS.bit.INTPRD = ET_1ST;           // Generate INT on 1st event
		
 */
	EPwm3Regs.TBPRD = uTsPerTdsp; // Period = 2��600 TBCLK counts
	EPwm3Regs.CMPA.half.CMPA = 0; // Compare A = 400 TBCLK counts
//	EPwm3Regs.CMPB = 3000; // Compare B = 500 TBCLK counts
	EPwm3Regs.TBPHS.all = 0; // Set Phase register to zero
	EPwm3Regs.TBCTR = 0; // clear TB counter

	EPwm3Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Symmetric
	EPwm3Regs.TBCTL.bit.PHSEN = TB_DISABLE; // Phase loading disabled
	EPwm3Regs.TBCTL.bit.PRDLD = TB_SHADOW;
	EPwm3Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO;
	EPwm3Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1; // TBCLK = SYSCLKOUT
	EPwm3Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	EPwm3Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm3Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm3Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO_PRD; // Freeze
	EPwm3Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO_PRD; // Freeze

	EPwm3Regs.AQCTLA.bit.CAU = AQ_CLEAR;
//	EPwm3Regs.AQCTLA.bit.CAD = AQ_SET;

	EPwm3Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
	EPwm3Regs.DBCTL.bit.POLSEL =  DB_ACTV_HI; // active low complementary

	EPwm3Regs.AQSFRC.bit.OTSFA = 1;
	EPwm3Regs.AQSFRC.bit.OTSFB = 1;

		//Sungho ADD
	EPwm3Regs.AQSFRC.bit.RLDCSF = 3;
	EPwm3Regs.AQSFRC.bit.ACTSFA = 1; //Clear (LOW)
	EPwm3Regs.AQSFRC.bit.ACTSFB = 1; //Clear (LOW)

	EPwm3Regs.DBCTL.bit.IN_MODE = DBA_ALL;
	EPwm3Regs.DBRED = EPWM3_DB;
	EPwm3Regs.DBFED = EPWM3_DB;

	EPwm3Regs.ETSEL.bit.INTSEL = ET_CTRU_CMPA;     // Select INT on Zero event
	EPwm3Regs.ETSEL.bit.INTEN = 1;		// Enable INT
	EPwm3Regs.ETPS.bit.INTPRD = ET_1ST;           // Generate INT on 1st event

/////////////////////////////////////////////////////////////////////////////////////////////////

	

}

void InitEpwm4(void)
{

	EALLOW;
//	EPwm4Regs.TZCLR.bit.CBC = 1;
//	EPwm4Regs.TZCLR.bit.INT = 1;
//	EPwm4Regs.TZCLR.bit.OST = 1;

//	EPwm4Regs.TZSEL.bit.OSHT2 = 1;
	EPwm4Regs.TZSEL.bit.CBC1 = 1;
	EPwm4Regs.TZCTL.bit.TZA = 2;	// Low state
	EPwm4Regs.TZCTL.bit.TZB = 2;	// Low state
//	EPwm4Regs.TZEINT.bit.OST = 1;
	EPwm4Regs.TZEINT.bit.CBC = 1;

	EDIS;

	EPwm4Regs.TBPRD = uTsPerTdsp; // Period = 2��600 TBCLK counts
	EPwm4Regs.CMPA.half.CMPA = 0; // Compare A = 400 TBCLK counts
	EPwm4Regs.TBPHS.all = 0; // Set Phase register to zero
	EPwm4Regs.TBCTR = 0; // clear TB counter

	EPwm4Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Symmetric
	EPwm4Regs.TBCTL.bit.PHSEN = TB_DISABLE; // Phase loading disabled
	EPwm4Regs.TBCTL.bit.PRDLD = TB_SHADOW;
	EPwm4Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO;
	EPwm4Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1; // TBCLK = SYSCLKOUT
	EPwm4Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	EPwm4Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm4Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm4Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO_PRD; // Freeze
	EPwm4Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO_PRD; // Freeze

	EPwm4Regs.AQCTLA.bit.CAU = AQ_CLEAR;
//	EPwm4Regs.AQCTLA.bit.CAD = AQ_SET;

	EPwm4Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
	EPwm4Regs.DBCTL.bit.POLSEL =  DB_ACTV_HI; // active low complementary
	EPwm4Regs.DBCTL.bit.IN_MODE = DBA_ALL;
	EPwm4Regs.DBRED = EPWM4_DB;
	EPwm4Regs.DBFED = EPWM4_DB;

	EPwm4Regs.ETSEL.bit.INTSEL = ET_CTRU_CMPA;     // Select INT on Zero event
	EPwm4Regs.ETSEL.bit.INTEN = 1;		// Enable INT
	EPwm4Regs.ETPS.bit.INTPRD = ET_1ST;           // Generate INT on 1st event

////////////////////////////////////////////////////////////////////////////////////////////////
}

void InitEpwm5(void)
{

	EALLOW;
//	EPwm5Regs.TZCLR.bit.CBC = 1;
//	EPwm5Regs.TZCLR.bit.INT = 1;
//	EPwm5Regs.TZCLR.bit.OST = 1;

//	EPwm5Regs.TZSEL.bit.OSHT2 = 1;
	EPwm5Regs.TZSEL.bit.CBC1 = 1;
	EPwm5Regs.TZCTL.bit.TZA = 2;	// Low state
	EPwm5Regs.TZCTL.bit.TZB = 2;	// Low state
//	EPwm5Regs.TZEINT.bit.OST = 1;
    EPwm5Regs.TZEINT.bit.CBC = 1;

	EDIS;

	EPwm5Regs.TBPRD = uTsPerTdsp; // Period = 2��600 TBCLK counts
	EPwm5Regs.CMPA.half.CMPA = 0; // Compare A = 400 TBCLK counts
	EPwm5Regs.TBPHS.all = 0; // Set Phase register to zero
	EPwm5Regs.TBCTR = 0; // clear TB counter
	EPwm5Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Symmetric
	EPwm5Regs.TBCTL.bit.PHSEN = TB_DISABLE; // Phase loading disabled
	EPwm5Regs.TBCTL.bit.PRDLD = TB_SHADOW;
	EPwm5Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO;
	EPwm5Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1; // TBCLK = SYSCLKOUT
	EPwm5Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	EPwm5Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm5Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm5Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO_PRD; // Freeze
	EPwm5Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO_PRD; // Freeze

	EPwm5Regs.AQCTLA.bit.CAU = AQ_CLEAR;
//	EPwm5Regs.AQCTLA.bit.CAD = AQ_SET;

	EPwm5Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
	EPwm5Regs.DBCTL.bit.POLSEL =  DB_ACTV_HI; // active low complementary
	EPwm5Regs.DBCTL.bit.IN_MODE = DBA_ALL;
	EPwm5Regs.DBRED = EPWM5_DB;
	EPwm5Regs.DBFED = EPWM5_DB;

	EPwm5Regs.ETSEL.bit.INTSEL = ET_CTRU_CMPA;     // Select INT on Zero event
	EPwm5Regs.ETSEL.bit.INTEN = 1;		// Enable INT
	EPwm5Regs.ETPS.bit.INTPRD = ET_1ST;           // Generate INT on 1st event

////////////////////////////////////////////////////////////////////////////////////////////////
}

void InitEpwm6(void)
{
	EALLOW;
//	EPwm6Regs.TZCLR.bit.CBC = 1;
//	EPwm6Regs.TZCLR.bit.INT = 1;
//	EPwm6Regs.TZCLR.bit.OST = 1;

//	EPwm6Regs.TZSEL.bit.OSHT2 = 1;
	EPwm6Regs.TZSEL.bit.CBC1 = 1;
	EPwm6Regs.TZCTL.bit.TZA = 2;	// Low state
	EPwm6Regs.TZCTL.bit.TZB = 2;	// Low state
//	EPwm6Regs.TZEINT.bit.OST = 1;
    EPwm6Regs.TZEINT.bit.CBC = 1;

	EDIS;

	EPwm6Regs.TBPRD = uTsPerTdsp; // Period = 2��600 TBCLK counts
	EPwm6Regs.CMPA.half.CMPA = 0; // Compare A = 400 TBCLK counts
	EPwm6Regs.TBPHS.all = 0; // Set Phase register to zero
	EPwm6Regs.TBCTR = 0; // clear TB counter

	EPwm6Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Symmetric
	EPwm6Regs.TBCTL.bit.PHSEN = TB_DISABLE; // Phase loading disabled
	EPwm6Regs.TBCTL.bit.PRDLD = TB_SHADOW;
	EPwm6Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO;
	EPwm6Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1; // TBCLK = SYSCLKOUT
	EPwm6Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	EPwm6Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm6Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm6Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO_PRD; // Freeze
	EPwm6Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO_PRD; // Freeze6
	EPwm6Regs.AQCTLA.bit.CAU = AQ_CLEAR;
//	EPwm6Regs.AQCTLA.bit.CAD = AQ_SET;

	EPwm6Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
	EPwm6Regs.DBCTL.bit.POLSEL =  DB_ACTV_HI; // active low complementary
	EPwm6Regs.DBCTL.bit.IN_MODE = DBA_ALL;
	EPwm6Regs.DBRED = EPWM6_DB;
	EPwm6Regs.DBFED = EPWM6_DB;

	EPwm6Regs.ETSEL.bit.INTSEL = ET_CTRU_CMPA;     // Select INT on Zero event
	EPwm6Regs.ETSEL.bit.INTEN = 1;		// Enable INT
	EPwm6Regs.ETPS.bit.INTPRD = ET_1ST;           // Generate INT on 1st event

///////////////////////////////////////////////////////////////////////////////////////////////
}

void InitEpwm7(void)
{

	EALLOW;

//110905 OSHT1 -> CBC2
//	EPwm7Regs.TZSEL.bit.OSHT1 = 1;
//	EPwm7Regs.TZSEL.bit.CBC1 = 1;
	EPwm7Regs.TZCTL.bit.TZA = 2;	// Low state
	EPwm7Regs.TZCTL.bit.TZB = 2;	// Low state
//	EPwm7Regs.TZEINT.bit.CBC = 1;
	EDIS;

	EPwm7Regs.TBPRD = uTsPerTdsp; // Period = 2��600 TBCLK counts
	EPwm7Regs.CMPA.half.CMPA = 0; // Compare A = 400 TBCLK counts
	EPwm7Regs.TBPHS.all = 0; // Set Phase register to zero
	EPwm7Regs.TBCTR = 0; // clear TB counter

	EPwm7Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Symmetric
	EPwm7Regs.TBCTL.bit.PHSEN = TB_DISABLE; // Phase loading disabled
	EPwm7Regs.TBCTL.bit.PRDLD = TB_SHADOW;
	EPwm7Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO;
	EPwm7Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1; // TBCLK = SYSCLKOUT
	EPwm7Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	EPwm7Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm7Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm7Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO_PRD; // Freeze
	EPwm7Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO_PRD; // Freeze

	EPwm7Regs.AQCTLA.bit.CAU = AQ_CLEAR;
//	EPwm7Regs.AQCTLA.bit.CAD = AQ_SET;

	EPwm7Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
	EPwm7Regs.DBCTL.bit.POLSEL =  DB_ACTV_HI; // active low complementary
	EPwm7Regs.DBCTL.bit.IN_MODE = DBA_ALL;
	EPwm7Regs.DBRED = EPWM7_DB;
	EPwm7Regs.DBFED = EPWM7_DB;

	EPwm7Regs.ETSEL.bit.INTSEL = ET_CTR_PRD;     // Select INT on Zero event
	EPwm7Regs.ETSEL.bit.INTEN = 1;		// Enable INT
	EPwm7Regs.ETPS.bit.INTPRD = ET_1ST;           // Generate INT on 1st event

///////////////////////////////////////////////////////////////////////////////////////////////

}

void InitEpwm8(void)
{
	EALLOW;

//110905 OSHT1 -> CBC2
//	EPwm8Regs.TZSEL.bit.OSHT1 = 1;
//	EPwm8Regs.TZSEL.bit.CBC1 = 1;
	EPwm8Regs.TZCTL.bit.TZA = 2;	// Low state
	EPwm8Regs.TZCTL.bit.TZB = 2;	// Low state
//	EPwm8Regs.TZEINT.bit.CBC = 1;
	EDIS;

//	EPwm8Regs.TBPRD = uTsPerTdsp*6; // Period = 2��600 TBCLK counts
//	EPwm8Regs.CMPA.half.CMPA = uTsPerTdsp*6; // Compare A = 400 TBCLK counts
	EPwm8Regs.TBPRD = uTsPerTdsp; // Period = 2��600 TBCLK counts
	EPwm8Regs.CMPA.half.CMPA = uTsPerTdsp; // Compare A = 400 TBCLK counts
	EPwm8Regs.TBPHS.all = 0; // Set Phase register to zero
	EPwm8Regs.TBCTR = 0; // clear TB counter

	EPwm8Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Symmetric
	EPwm8Regs.TBCTL.bit.PHSEN = TB_DISABLE; // Phase loading disabled
	EPwm8Regs.TBCTL.bit.PRDLD = TB_SHADOW;
	EPwm8Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO;
	EPwm8Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1; // TBCLK = SYSCLKOUT
	EPwm8Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	EPwm8Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm8Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm8Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO_PRD; // Freeze
	EPwm8Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO_PRD; // Freeze

	EPwm8Regs.AQCTLA.bit.CAU = AQ_CLEAR;
//	EPwm8Regs.AQCTLA.bit.CAD = AQ_SET;

	EPwm8Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
	EPwm8Regs.DBCTL.bit.POLSEL =  DB_ACTV_HI; // active low complementary
	EPwm8Regs.DBCTL.bit.IN_MODE = DBA_ALL;
	EPwm8Regs.DBRED = EPWM8_DB;
	EPwm8Regs.DBFED = EPWM8_DB;

	EPwm8Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;     // Select INT on Zero event
	EPwm8Regs.ETSEL.bit.INTEN = 0;		// Enable INT
	EPwm8Regs.ETPS.bit.INTPRD = ET_1ST;           // Generate INT on 1st event

/////////////////////////////////////////////////////////////////////////////////////////////// 
}

void InitEpwm9(void)
{
	EALLOW;

//110905 OSHT1 -> CBC2
//	EPwm9Regs.TZSEL.bit.OSHT1 = 1;
//	EPwm9Regs.TZSEL.bit.CBC1 = 1;
	EPwm9Regs.TZCTL.bit.TZA = 2;	// Low state
	EPwm9Regs.TZCTL.bit.TZB = 2;	// Low state
//	EPwm9Regs.TZEINT.bit.CBC = 1;
	EDIS;

	EPwm9Regs.TBPRD = uTsPerTdsp; // Period = 2��600 TBCLK counts
	EPwm9Regs.CMPA.half.CMPA = 0; // Compare A = 400 TBCLK counts
	EPwm9Regs.TBPHS.all = 0; // Set Phase register to zero
	EPwm9Regs.TBCTR = 0; // clear TB counter

	EPwm9Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; // Symmetric
	EPwm9Regs.TBCTL.bit.PHSEN = TB_DISABLE; // Phase loading disabled
	EPwm9Regs.TBCTL.bit.PRDLD = TB_SHADOW;
	EPwm9Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO;
	EPwm9Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1; // TBCLK = SYSCLKOUT
	EPwm9Regs.TBCTL.bit.CLKDIV = TB_DIV1;

	EPwm9Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
	EPwm9Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	EPwm9Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO_PRD; // Freeze
	EPwm9Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO_PRD; // Freeze

	EPwm9Regs.AQCTLA.bit.CAU = AQ_CLEAR;
//	EPwm9Regs.AQCTLA.bit.CAD = AQ_SET;

	EPwm9Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
	EPwm9Regs.DBCTL.bit.POLSEL =  DB_ACTV_HI; // active low complementary
	EPwm9Regs.DBCTL.bit.IN_MODE = DBA_ALL;
	EPwm9Regs.DBRED = EPWM9_DB;
	EPwm9Regs.DBFED = EPWM9_DB;

	EPwm9Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;     // Select INT on Zero event
	EPwm9Regs.ETSEL.bit.INTEN = 1;		// Enable INT
	EPwm9Regs.ETPS.bit.INTPRD = ET_1ST;           // Generate INT on 1st event

/////////////////////////////////////////////////////////////////////////////////////////////// 

}



//---------------------------------------------------------------------------
// Example: InitEPwmGpio:
//---------------------------------------------------------------------------
// This function initializes GPIO pins to function as ePWM pins
//
// Each GPIO pin can be configured as a GPIO pin or up to 3 different
// peripheral functional pins. By default all pins come up as GPIO
// inputs after reset.
//
void InitEpwm(void)
{
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;      // Stop all the TB clocks
	EDIS;
	InitEpwm1();
	InitEpwm2();
	InitEpwm3();
	InitEpwm4();
	InitEpwm5();
	InitEpwm6();
	InitEpwm7();
	InitEpwm8();
//	InitEpwm9();

	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;         // Start all the timers synced
	EDIS;
}


void InitEPwmGpio(void)
{
   InitEPwm1Gpio();
   InitEPwm2Gpio();
   InitEPwm3Gpio();
#if DSP28_EPWM4
   InitEPwm4Gpio();
#endif // endif DSP28_EPWM4
#if DSP28_EPWM5
   InitEPwm5Gpio();
#endif // endif DSP28_EPWM5
#if DSP28_EPWM6
   InitEPwm6Gpio();
#endif // endif DSP28_EPWM6
//#if DSP28_EPWM7
   InitEPwm7Gpio();
//#endif // endif DSP28_EPWM7
//#if DSP28_EPWM8
   InitEPwm8Gpio();
//#endif // endif DSP28_EPWM8

  //for techwin  EPWM9-->gpio(relay_con)
   //#if DSP28_EPWM9
   // InitEPwm9Gpio();
   //#endif // endif DSP28_EPWM9

}

void InitEPwm1Gpio(void)
{
   EALLOW;

/* Disable internal pull-up for the selected output pins
   to reduce power consumption */
// Pull-ups can be enabled or disabled by the user.
// Comment out other unwanted lines.

    GpioCtrlRegs.GPAPUD.bit.GPIO0 = 1;    // Disable pull-up on GPIO0 (EPWM1A)
    GpioCtrlRegs.GPAPUD.bit.GPIO1 = 1;    // Disable pull-up on GPIO1 (EPWM1B)

/* Configure ePWM-1 pins using GPIO regs*/
// This specifies which of the possible GPIO pins will be ePWM1 functional pins.
// Comment out other unwanted lines.

    GpioCtrlRegs.GPAMUX1.bit.GPIO0 = 1;   // Configure GPIO0 as EPWM1A
    GpioCtrlRegs.GPAMUX1.bit.GPIO1 = 1;   // Configure GPIO1 as EPWM1B

    EDIS;
}

void InitEPwm2Gpio(void)
{
   EALLOW;

/* Disable internal pull-up for the selected output pins
   to reduce power consumption */
// Pull-ups can be enabled or disabled by the user.
// Comment out other unwanted lines.

    GpioCtrlRegs.GPAPUD.bit.GPIO2 = 1;    // Disable pull-up on GPIO2 (EPWM2A)
    GpioCtrlRegs.GPAPUD.bit.GPIO3 = 1;    // Disable pull-up on GPIO3 (EPWM3B)

/* Configure ePWM-2 pins using GPIO regs*/
// This specifies which of the possible GPIO pins will be ePWM2 functional pins.
// Comment out other unwanted lines.

    GpioCtrlRegs.GPAMUX1.bit.GPIO2 = 1;   // Configure GPIO2 as EPWM2A
    GpioCtrlRegs.GPAMUX1.bit.GPIO3 = 1;   // Configure GPIO3 as EPWM2B

    EDIS;
}

void InitEPwm3Gpio(void)
{
   EALLOW;

/* Disable internal pull-up for the selected output pins
   to reduce power consumption */
// Pull-ups can be enabled or disabled by the user.
// Comment out other unwanted lines.

    GpioCtrlRegs.GPAPUD.bit.GPIO4 = 1;    // Disable pull-up on GPIO4 (EPWM3A)
    GpioCtrlRegs.GPAPUD.bit.GPIO5 = 1;    // Disable pull-up on GPIO5 (EPWM3B)

/* Configure ePWM-3 pins using GPIO regs*/
// This specifies which of the possible GPIO pins will be ePWM3 functional pins.
// Comment out other unwanted lines.

    GpioCtrlRegs.GPAMUX1.bit.GPIO4 = 1;   // Configure GPIO4 as EPWM3A
    GpioCtrlRegs.GPAMUX1.bit.GPIO5 = 1;   // Configure GPIO5 as EPWM3B

    EDIS;
}


#if DSP28_EPWM4
void InitEPwm4Gpio(void)
{
   EALLOW;
/* Disable internal pull-up for the selected output pins
   to reduce power consumption */
// Pull-ups can be enabled or disabled by the user.
// Comment out other unwanted lines.

    GpioCtrlRegs.GPAPUD.bit.GPIO6 = 1;    // Disable pull-up on GPIO6 (EPWM4A)
    GpioCtrlRegs.GPAPUD.bit.GPIO7 = 1;    // Disable pull-up on GPIO7 (EPWM4B)

/* Configure ePWM-4 pins using GPIO regs*/
// This specifies which of the possible GPIO pins will be ePWM4 functional pins.
// Comment out other unwanted lines.

    GpioCtrlRegs.GPAMUX1.bit.GPIO6 = 1;   // Configure GPIO6 as EPWM4A
    GpioCtrlRegs.GPAMUX1.bit.GPIO7 = 1;   // Configure GPIO7 as EPWM4B

    EDIS;
}
#endif // endif DSP28_EPWM4


#if DSP28_EPWM5
void InitEPwm5Gpio(void)
{
   EALLOW;
/* Disable internal pull-up for the selected output pins
   to reduce power consumption */
// Pull-ups can be enabled or disabled by the user.
// Comment out other unwanted lines.

    GpioCtrlRegs.GPAPUD.bit.GPIO8 = 1;    // Disable pull-up on GPIO8 (EPWM5A)
    GpioCtrlRegs.GPAPUD.bit.GPIO9 = 1;    // Disable pull-up on GPIO9 (EPWM5B)

/* Configure ePWM-5 pins using GPIO regs*/
// This specifies which of the possible GPIO pins will be ePWM5 functional pins.
// Comment out other unwanted lines.

    GpioCtrlRegs.GPAMUX1.bit.GPIO8 = 1;   // Configure GPIO8 as EPWM5A
    GpioCtrlRegs.GPAMUX1.bit.GPIO9 = 1;   // Configure GPIO9 as EPWM5B

    EDIS;
}
#endif // endif DSP28_EPWM5


#if DSP28_EPWM6
void InitEPwm6Gpio(void)
{
   EALLOW;

/* Disable internal pull-up for the selected output pins
   to reduce power consumption */
// Pull-ups can be enabled or disabled by the user.
// Comment out other unwanted lines.

    GpioCtrlRegs.GPAPUD.bit.GPIO10 = 1;    // Disable pull-up on GPIO10 EPWM6A
    GpioCtrlRegs.GPAPUD.bit.GPIO11 = 1;    // Disable pull-up on GPIO11 EPWM6B

/* Configure ePWM-6 pins using GPIO regs*/
// This specifies which of the possible GPIO pins will be ePWM6 functional pins.
// Comment out other unwanted lines.

    GpioCtrlRegs.GPAMUX1.bit.GPIO10 = 1;   // Configure GPIO10 as EPWM6A
    GpioCtrlRegs.GPAMUX1.bit.GPIO11 = 1;   // Configure GPIO11 as EPWM6B

    EDIS;
}
#endif // endif DSP28_EPWM6

//#if DSP28_EPWM7
void InitEPwm7Gpio(void)
{
   EALLOW;

/* Disable internal pull-up for the selected output pins
   to reduce power consumption */
// Pull-ups can be enabled or disabled by the user.
// Comment out other unwanted lines.

    GpioCtrlRegs.GPBPUD.bit.GPIO58 = 1;    // Disable pull-up on GPIO58 EPWM7A
    GpioCtrlRegs.GPBPUD.bit.GPIO59 = 1;    // Disable pull-up on GPIO59 EPWM7B

/* Configure ePWM-7 pins using GPIO regs*/
// This specifies which of the possible GPIO pins will be ePWM6 functional pins.
// Comment out other unwanted lines.

    GpioCtrlRegs.GPBMUX2.bit.GPIO58 = 3;   // Configure GPIO58 as EPWM7A
    GpioCtrlRegs.GPBMUX2.bit.GPIO59 = 3;   // Configure GPIO59 as EPWM7B

    EDIS;
}
//#endif // endif DSP28_EPWM7

//#if DSP28_EPWM8
void InitEPwm8Gpio(void)
{
   EALLOW;

/* Disable internal pull-up for the selected output pins
   to reduce power consumption */
// Pull-ups can be enabled or disabled by the user.
// Comment out other unwanted lines.

    GpioCtrlRegs.GPBPUD.bit.GPIO60 = 1;    // Disable pull-up on GPIO60 EPWM8A
    GpioCtrlRegs.GPBPUD.bit.GPIO61 = 1;    // Disable pull-up on GPIO61 EPWM8B

/* Disable internal pull-up for the selected output pins
   to reduce power consumption */
// This specifies which of the possible GPIO pins will be ePWM6 functional pins.
// Comment out other unwanted lines.

    GpioCtrlRegs.GPBMUX2.bit.GPIO60 = 3;   // Configure GPIO60 as EPWM8A
    GpioCtrlRegs.GPBMUX2.bit.GPIO61 = 3;   // Configure GPIO61 as EPWM8B

    EDIS;
}
//#endif // endif DSP28_EPWM8

//#if DSP28_EPWM9
void InitEPwm9Gpio(void)
{
   EALLOW;

/* Disable internal pull-up for the selected output pins
   to reduce power consumption */
// Pull-ups can be enabled or disabled by the user.
// Comment out other unwanted lines.

//    GpioCtrlRegs.GPBPUD.bit.GPIO62 = 1;    // Disable pull-up on GPIO62 EPWM9A
    GpioCtrlRegs.GPBPUD.bit.GPIO63 = 1;    // Disable pull-up on GPIO63 EPWM9B

/* Configure ePWM-9 pins using GPIO regs*/
// This specifies which of the possible GPIO pins will be ePWM6 functional pins.
// Comment out other unwanted lines.

//    GpioCtrlRegs.GPBMUX2.bit.GPIO62 = 3;   // Configure GPIO62 as EPWM9A
    GpioCtrlRegs.GPBMUX2.bit.GPIO63 = 3;   // Configure GPIO63 as EPWM9B

    EDIS;
}
//#endif // endif DSP28_EPWM9

//---------------------------------------------------------------------------
// Example: InitEPwmSyncGpio:
//---------------------------------------------------------------------------
// This function initializes GPIO pins to function as ePWM Synch pins
//

void InitEPwmSyncGpio(void)
{

   EALLOW;

/* Configure EPWMSYNCI  */

/* Enable internal pull-up for the selected pins */
// Pull-ups can be enabled or disabled by the user.
// This will enable the pullups for the specified pins.
// Comment out other unwanted lines.

   GpioCtrlRegs.GPAPUD.bit.GPIO6 = 0;    // Enable pull-up on GPIO6 (EPWMSYNCI)
// GpioCtrlRegs.GPBPUD.bit.GPIO32 = 0;   // Enable pull-up on GPIO32 (EPWMSYNCI)

/* Set qualification for selected pins to asynch only */
// This will select synch to SYSCLKOUT for the selected pins.
// Comment out other unwanted lines.

   GpioCtrlRegs.GPAQSEL1.bit.GPIO6 = 0;   // Synch to SYSCLKOUT GPIO6 (EPWMSYNCI)
// GpioCtrlRegs.GPBQSEL1.bit.GPIO32 = 0;  // Synch to SYSCLKOUT GPIO32 (EPWMSYNCI)

/* Configure EPwmSync pins using GPIO regs*/
// This specifies which of the possible GPIO pins will be EPwmSync functional pins.
// Comment out other unwanted lines.

   GpioCtrlRegs.GPAMUX1.bit.GPIO6 = 2;    // Enable pull-up on GPIO6 (EPWMSYNCI)
// GpioCtrlRegs.GPBMUX1.bit.GPIO32 = 2;   // Enable pull-up on GPIO32 (EPWMSYNCI)



/* Configure EPWMSYNC0  */

/* Disable internal pull-up for the selected output pins
   to reduce power consumption */
// Pull-ups can be enabled or disabled by the user.
// Comment out other unwanted lines.

// GpioCtrlRegs.GPAPUD.bit.GPIO6 = 1;    // Disable pull-up on GPIO6 (EPWMSYNC0)
   GpioCtrlRegs.GPBPUD.bit.GPIO33 = 1;   // Disable pull-up on GPIO33 (EPWMSYNC0)

// GpioCtrlRegs.GPAMUX1.bit.GPIO6 = 3;    // Enable pull-up on GPIO6 (EPWMSYNC0)
   GpioCtrlRegs.GPBMUX1.bit.GPIO33 = 2;   // Enable pull-up on GPIO33 (EPWMSYNC0)

}



//---------------------------------------------------------------------------
// Example: InitTzGpio:
//---------------------------------------------------------------------------
// This function initializes GPIO pins to function as Trip Zone (TZ) pins
//
// Each GPIO pin can be configured as a GPIO pin or up to 3 different
// peripheral functional pins. By default all pins come up as GPIO
// inputs after reset.
//

void InitTzGpio(void)
{
   EALLOW;

/* Enable internal pull-up for the selected pins */
// Pull-ups can be enabled or disabled by the user.
// This will enable the pullups for the specified pins.
// Comment out other unwanted lines.
   GpioCtrlRegs.GPAPUD.bit.GPIO12 = 0;    // Enable pull-up on GPIO12 (TZ1)
   GpioCtrlRegs.GPAPUD.bit.GPIO13 = 0;    // Enable pull-up on GPIO13 (TZ2)
   GpioCtrlRegs.GPAPUD.bit.GPIO14 = 0;    // Enable pull-up on GPIO14 (TZ3)
   GpioCtrlRegs.GPAPUD.bit.GPIO15 = 0;    // Enable pull-up on GPIO15 (TZ4)

   GpioCtrlRegs.GPAPUD.bit.GPIO16 = 0;    // Enable pull-up on GPIO16 (TZ5)
// GpioCtrlRegs.GPAPUD.bit.GPIO28 = 0;    // Enable pull-up on GPIO28 (TZ5)

   GpioCtrlRegs.GPAPUD.bit.GPIO17 = 0;    // Enable pull-up on GPIO17 (TZ6)
// GpioCtrlRegs.GPAPUD.bit.GPIO29 = 0;    // Enable pull-up on GPIO29 (TZ6)

/* Set qualification for selected pins to asynch only */
// Inputs are synchronized to SYSCLKOUT by default.
// This will select asynch (no qualification) for the selected pins.
// Comment out other unwanted lines.

   GpioCtrlRegs.GPAQSEL1.bit.GPIO12 = 3;  // Asynch input GPIO12 (TZ1)
   GpioCtrlRegs.GPAQSEL1.bit.GPIO13 = 3;  // Asynch input GPIO13 (TZ2)
   GpioCtrlRegs.GPAQSEL1.bit.GPIO14 = 3;  // Asynch input GPIO14 (TZ3)
   GpioCtrlRegs.GPAQSEL1.bit.GPIO15 = 3;  // Asynch input GPIO15 (TZ4)

   GpioCtrlRegs.GPAQSEL2.bit.GPIO16 = 3;  // Asynch input GPIO16 (TZ5)
// GpioCtrlRegs.GPAQSEL2.bit.GPIO28 = 3;  // Asynch input GPIO28 (TZ5)

   GpioCtrlRegs.GPAQSEL2.bit.GPIO17 = 3;  // Asynch input GPIO17 (TZ6)
// GpioCtrlRegs.GPAQSEL2.bit.GPIO29 = 3;  // Asynch input GPIO29 (TZ6)


/* Configure TZ pins using GPIO regs*/
// This specifies which of the possible GPIO pins will be TZ functional pins.
// Comment out other unwanted lines.
   GpioCtrlRegs.GPAMUX1.bit.GPIO12 = 1;  // Configure GPIO12 as TZ1
   GpioCtrlRegs.GPAMUX1.bit.GPIO13 = 1;  // Configure GPIO13 as TZ2
   GpioCtrlRegs.GPAMUX1.bit.GPIO14 = 1;  // Configure GPIO14 as TZ3
//   GpioCtrlRegs.GPAMUX1.bit.GPIO15 = 1;  // Configure GPIO15 as TZ4

   GpioCtrlRegs.GPAMUX2.bit.GPIO16 = 3;  // Configure GPIO16 as TZ5
// GpioCtrlRegs.GPAMUX2.bit.GPIO28 = 3;  // Configure GPIO28 as TZ5

   GpioCtrlRegs.GPAMUX2.bit.GPIO17 = 3;  // Configure GPIO17 as TZ6
// GpioCtrlRegs.GPAMUX2.bit.GPIO29 = 3;  // Configure GPIO29 as TZ6

   EDIS;
}
/*
void InitEPwm(void)
{
	
	// 2. Set TBCLKSYNC to 0.
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;      // Stop all the TB clocks
	EDIS;

	// 3. Configure prescaler values and ePWM modes.
	InitEPwm1();
//	InitEPwm2();
//	InitEPwm3();
//	InitEPwm4();
//	InitEPwm5();
//	InitEPwm6();

	// 4. Set TBCLKSYNC(Time Base Clock) to 1.
	EALLOW;
	SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;         // Start all the timers synced
	EDIS;
 
}



void InitEPwm1(void)
{
	
	// Trip-Zone
	EALLOW;
	// OSHT1
	// Trip-zone 1 (TZ1) Select
	//		0 : Disable TZ1 as a one-shot trip source for this ePWM module
	//		1 : Enable TZ1 as a one-shot trip source for this ePWM module
	EPwm1Regs.TZSEL.bit.OSHT1 = 1;
	EPwm1Regs.TZSEL.bit.CBC2 = 1;
	// CBC1
	// Trip-zone 1 (TZ1) Select
	//		0 : Disable TZ1 as a CBC trip source for this ePWM module
	//		1 : Enable TZ1 as a CBC trip source for this ePWM module
//	EPwm1Regs.TZSEL.bit.CBC1 = 1;
	// TZA [1..0] / TZB [3..2]
	// When a trip event occurs the following action is taken on output EPWMxA/EPWMxB.
	// Which trip-zone pins can cause an event is defined in the TZSEL register.
	//		00 : High impedance (EPWMxA/EPWMxB = High-impedance state)
	//		01 : Force EPWMxA/EPWMxB to a high state
	//		10 : Force EPWMxA/EPWMxB to a low state
	//		11 : Do nothing, no action is taken on EPWMxA/EPWMxB.
	EPwm1Regs.TZCTL.bit.TZA = 2;	// Low state
	EPwm1Regs.TZCTL.bit.TZB = 2;	// Low state
	// OST
	// Trip-zone One-Shot Interrupt Enable
	//		0 : Disable one-shot interrupt generation
	//		1 : Enable Interrupt generation;
	//			a one-shot trip event will cause a EPWMx_TZINT PIE interrupt.
	EPwm1Regs.TZEINT.bit.OST = 1;
//	EPwm1Regs.TZFRC.bit.OST = PWM1_STOP;
	EDIS;


	// Period
	// Time-Base Period Register(TBPRD)
	// These bits determine the period of the time-base counter. 
	// This sets the PWM frequency.
	// Shadowing of this register is enabled and disabled by the TBCTL[PRDLD] bit. 
	// By default this register is shadowed.
	// ��If TBCTL[PRDLD] = 0, then the shadow is enabled and any write or read
	//	 will automatically go to the shadow register. In this case, the active register
	//	 will be loaded from the shadow register when the time-base counter equals zero.
	// ��If TBCTL[PRDLD] = 1, then the shadow is disabled and any write or read
	//	 will go directly to the active register, that is the register actively
	//	 controlling the hardware.
	// ��The active and shadow registers share the same memory map address.
	EPwm1Regs.TBPRD = Ts_cnt; ///  PWM1_TIMER_TBPRD;				// Set timer period
	// These bits set time-base counter phase of the selected ePWM 
	// relative to the time-base that is supplying the synchronization input signal.
	// ��If TBCTL[PHSEN] = 0, then the synchronization event is ignored 
	//	 and the time-base counter is not loaded with the phase.
	// ��If TBCTL[PHSEN] = 1, then the time-base counter (TBCTR) will be loaded 
	//	 with the phase(TBPHS) when a synchronization event occurs. 
	//	 The synchronization event can be initiated by the input synchronization signal
	//	 (EPWMxSYNCI) or by a software forced synchronization.
	EPwm1Regs.TBPHS.half.TBPHS = 0x0000;           	// Phase is 0
	EPwm1Regs.TBCTR = 0x0000;                      	// Clear counter

	// Setup Time-Base Submodule Registers
	// Time-Base Control Register(TBCTL) : Counter Mode(CTRMODE)
	// The time-base counter mode is normally configured once and not changed 
	// during normal operation.
	// If you change the mode of the counter, the change will take effect 
	// at the next TBCLK edge and the current counter value shall increment or decrement
	// from the value before the mode change.
	// These bits set the time-base counter mode of operation as follows:
	//		00 : Up-count mode
	//		01 : Down-count mode
	//		10 : Up-down-count mode
	//		11 : Stop-freeze counter operation (default on reset)
	EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN; 	// Up-down-count mode
	// Counter Register Load From Phase Register Enable
	// 0 : Do not load the time-base counter (TBCTR) from the 
	//	   time-base phase register (TBPHS)
	// 1 : Load the time-base counter with the phase register 
	//	   when an EPWMxSYNCI input signal occurs or
	//	   when a software synchronization is forced by the SWFSYNC bit.
	EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE;			// Disable phase loading. Mastser Module
	EPwm1Regs.TBCTL.bit.PRDLD = TB_SHADOW;   // shadowing enabled
	// Setup Sync
	// Synchronization Output Select. 
	// These bits select the source of the EPWMxSYNCO signal.
	//
	// 00 : EPWMxSYNC
	// 01 : CTR = zero: Time-base counter equal to zero (TBCTR = 0x0000)
	// 10 : CTR = CMPB : Time-base counter equal to counter-compare B (TBCTR = CMPB)
	// 11 : Disable EPWMxSYNCO signal
	EPwm1Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO;
	// High Speed Time-base Clock Prescale Bits
	// These bits determine part of the time-base clock prescale value.
	// TBCLK = SYSCLKOUT / (HSPCLKDIV * CLKDIV)
	// This divisor emulates the HSPCLK in the TMS320x281x system
	// as used on the Event Manager(EV) peripheral.
	// 000 : /1, 001 : /2 (default on reset), 010 : /4,  011 : /6
	// 100 : /8, 101 : /10, 				  110 : /12, 111 : /14
	EPwm1Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;		// Clock ratio to SYSCLKOUT
	// Time-base Clock Prescale Bits
	// These bits determine part of the time-base clock prescale value.
	// TBCLK = SYSCLKOUT / (HSPCLKDIV * CLKDIV)
	// 000 : /1 (default on reset), 001 : /2,  010 : /4,  011 : /8
	// 100 : /16, 					101 : /32, 110 : /64, 111 : /128
	EPwm1Regs.TBCTL.bit.CLKDIV = TB_DIV1;
	// -> TBCLK = 150MHz / (1 * 1) = 150MHz : 15000 count = 0.1ms
//	EPwm1Regs.TBCTL.all = 0x0012;	// 00 0 000 000 0 01 0 0 10		0000 0000 0001 0010

	// Setup Counter-Compare Submodule Registers
	// SHDWAMODE : Counter-compare A (CMPA) Register Operating Mode
	// SHDWBMODE : Counter-compare B (CMPB) Register Operating Mode
	//		0 : Shadow mode. Operates as a double buffer. 
	//			All writes via the CPU access the shadow register.
	//		1 : Immediate mode. Only the active compare register is used. 
	//			All writes and reads directly access the active register for immediate compare action
	EPwm1Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;    // Load registers every ZERO
	EPwm1Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
	// LOADAMODE : Active Counter-Compare A (CMPA) Load From Shadow Select Mode.
	//			   This bit has no effect in immediate mode (CMPCTL[SHDWAMODE] = 1).
	// LOADBMODE : Active Counter-Compare B (CMPB) Load From Shadow Select Mode
	// 			   This bit has no effect in immediate mode (CMPCTL[SHDWBMODE] = 1).
	//		00 : Load on CTR = Zero: Time-base counter equal to zero (TBCTR = 0x0000)
	//		01 : Load on CTR = PRD: Time-base counter equal to period (TBCTR = TBPRD)
	//		10 : Load on either CTR = Zero or CTR = PRD
	//		11 : Freeze (no loads possible)
	EPwm1Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO_PRD;
	EPwm1Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO_PRD;

	// Setup compare
	EPwm1Regs.CMPA.half.CMPA = HalfDuty;

	// Set actions
	// CAU : Action when the counter equals the active CMPA register and
	//		 the counter is incrementing.
	// CAD : Action when the counter equals the active CMPA register and
	//		 the counter is decrementing.
	//		00 : Do nothing (action disabled)
	//		01 : Clear: force EPWMxA output low.
	//		10 : Set: force EPWMxA output high.
	//		11 : Toggle EPWMxA output: low output signal will be forced high,
	//			 and a high signal will be forced low.
	// cf.spru791d
	//	TMS320x28xx, 28xxx Enhanced Pulse Width Modulator (ePWM) Module (Rev_D).pdf
	//	page 48, Figure 2-25, Example 2-6
	// Up-Down-Count, Dual Edge Symmetric Waveform,
	// With Independent Modulation on EPWMxA and EPWMxB - Complementary
	EPwm1Regs.AQCTLA.bit.CAU = AQ_CLEAR;             // Set PWM1A on Zero
	EPwm1Regs.AQCTLA.bit.CAD = AQ_SET;




//	EPwm1Regs.AQCTLB.bit.CAU = AQ_SET;          // Set PWM1B on Zero
//	EPwm1Regs.AQCTLB.bit.CAD = AQ_CLEAR;

	// Active High PWMs - Setup Deadband
	// OUT_MODE : Dead-band Output Mode Control
	// Bit 1 controls the S1 switch and bit 0 controls the S0 switch.
	// This allows you to selectively enable or bypass the dead-band generation for the falling-edge
	// and rising-edge delay.
	//		00 : Dead-band generation is bypassed for both output signals.
	//			 In this mode, both the EPWMxA and EPWMxB output signals from the action-qualifier
	//			 are passed directly to the PWM-chopper submodule.
	//			 In this mode, the POLSEL and IN_MODE bits have no effect.
	//		01 : Disable rising-edge delay. The EPWMxA signal from the action-qualifier is passed
	//			 straight through to the EPWMxA input of the PWM-chopper submodule.
	//			 The falling-edge delayed signal is seen on output EPWMxB.
	//			 The input signal for the delay is determined by DBCTL[IN_MODE].
	//		10 : The rising-edge delayed signal is seen on output EPWMxA.
	//			 The input signal for the delay is determined by DBCTL[IN_MODE].
	//			 Disable falling-edge delay. The EPWMxB signal from the action-qualifier is passed
	//			 straight through to the EPWMxB input of the PWM-chopper submodule.
	//		11 : Dead-band is fully enabled for both rising-edge delay on output EPWMxA
	//			 and falling-edge delay on output EPWMxB.
	//			 The input signal for the delay is determined by DBCTL[IN_MODE].
	EPwm1Regs.DBCTL.bit.OUT_MODE = DB_FULL_ENABLE;
	// POLSEL : Polarity Select Control
	// Bit 3 controls the S3 switch and bit 2 controls the S2 switch.
	// This allows you to selectively invert one of the delayed signals
	// before it is sent out of the dead-band submodule.
	// The following descriptions correspond to classical upper/lower switch control
	// as found in one leg of a digital motor control inverter.
	// These assume that DBCTL[OUT_MODE] = 1,1 and DBCTL[IN_MODE] = 0,0. Other
	// enhanced modes are also possible, but not regarded as typical usage modes.
	//		00 : Active high (AH) mode. Neither EPWMxA nor EPWMxB is inverted (default).
	//		01 : Active low complementary (ALC) mode. EPWMxA is inverted.
	//		10 : Active high complementary (AHC). EPWMxB is inverted.
	//		11 : Active low (AL) mode. Both EPWMxA and EPWMxB are inverted.
	EPwm1Regs.DBCTL.bit.POLSEL =  DB_ACTV_LOC; // active low complementary
	EPwm1Regs.DBCTL.bit.IN_MODE = DBA_ALL;
	EPwm1Regs.DBRED = EPWM1_DB;
	EPwm1Regs.DBFED = EPWM1_DB;


	// Interrupt where we will change the Deadband
	// Event-Trigger Selection Register(ETSEL)
	//					: ePWM Interrupt (EPWMx_INT) Selection Options(INTSEL)
	//		000 : Reserved
	//		001 : Enable event time-base counter equal to zero. (TBCTR = 0x0000)
	//		010 : Enable event time-base counter equal to period (TBCTR = TBPRD)
	//		011 : Reserved
	//		100 : Enable event time-base counter equal to CMPA when the timer is incrementing.
	//		101 : Enable event time-base counter equal to CMPA when the timer is decrementing.
	//		110 : Enable event: time-base counter equal to CMPB when the timer is incrementing.
	//		111 : Enable event: time-base counter equal to CMPB when the timer is decrementing.
	EPwm1Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;     // Select INT on Zero event
	// Event-Trigger Selection Register(ETSEL)
	//					: Enable ePWM Interrupt (EPWMx_INT) Generation(INTEN)
	//		0 : Disable EPWMx_INT generation
	//		1 : Enable EPWMx_INT generation
	EPwm1Regs.ETSEL.bit.INTEN = 1;//PWM1_INT_ENABLE;		// Enable INT
	// SOCAEN : Enable the ADC Start of Conversion A (EPWMxSOCA) Pulse
	//		0 : Disable EPWMxSOCA.
	//		1 : Enable EPWMxSOCA pulse.
	EPwm1Regs.ETSEL.bit.SOCAEN = 1;        // Enable SOC on A group
	// SOCASEL : EPWMxSOCA Selection Options
	//			 These bits determine when a EPWMxSOCA pulse will be generated.
	//		000 : Reserved
	//		001 : Enable event time-base counter equal to zero. (TBCTR = 0x0000)
	//		010 : Enable event time-base counter equal to period (TBCTR = TBPRD)
	//		011 : Reserved
	//		100 : Enable event time-base counter equal to CMPA when the timer is incrementing.
	//		101 : Enable event time-base counter equal to CMPA when the timer is decrementing.
	//		110 : Enable event: time-base counter equal to CMPB when the timer is incrementing.
	//		111 : Enable event: time-base counter equal to CMPB when the timer is decrementing.
	EPwm1Regs.ETSEL.bit.SOCASEL = ET_CTR_ZERO;       // Select SOC from from CPMA on upcount

	// Event-Trigger Prescale Register(ETPS)
	//					: ePWM Interrupt (EPWMx_INT) Period Select(INTPRD)
	// These bits determine how many selected ETSEL[INTSEL] events need to occur
	// before an interrupt is generated. To be generated, the interrupt must be enabled
	// (ETSEL[INT] = 1). If the interrupt status flag is set from a previous interrupt
	// (ETFLG[INT] = 1) then no interrupt will be generated until the flag is cleared
	// via the ETCLR[INT] bit. This allows for one interrupt to be pending while
	// another is still being serviced. Once the interrupt is generated,
	// the ETPS[INTCNT] bits will automatically be cleared.
	// Writing a INTPRD value that is the same as the current counter value
	// will trigger an interrupt if it is enabled and the status flag is clear.
	// Writing a INTPRD value that is less than the current counter value
	// will result in an undefined state.
	// If a counter event occurs at the same instant as a new zero or non-zero INTPRD value
	// is written, the counter is incremented.
	//		00 : Disable the interrupt event counter.
	//			 No interrupt will be generated and ETFRC[INT] is ignored.
	//		01 : Generate an interrupt on the first event INTCNT = 01 (first event)
	//		10 : Generate interrupt on ETPS[INTCNT] = 1,0 (second event)
	//		11 : Generate interrupt on ETPS[INTCNT] = 1,1 (third event)
	EPwm1Regs.ETPS.bit.INTPRD = ET_1ST;           // Generate INT on 1st event
	// SOCAPRD : ePWM ADC Start-of-Conversion A Event (EPWMxSOCA) Period Select
	//			 These bits determine how many selected ETSEL[SOCASEL] events need to occur
	//			 before an EPWMxSOCA pulse is generated. To be generated, the pulse must be
	//			 enabled (ETSEL[SOCAEN] = 1). The SOCA pulse will be generated even if
	//			 the status flag is set from a previous start of conversion (ETFLG[SOCA] = 1).
	//			 Once the SOCA pulse is generated, the ETPS[SOCACNT] bits will automatically
	//			 be cleared.
	//		00 : Disable the SOCA event counter. No EPWMxSOCA pulse will be generated
	//		01 : Generate the EPWMxSOCA pulse on the first event: ETPS[SOCACNT] = 0,1
	//		10 : Generate the EPWMxSOCA pulse on the second event: ETPS[SOCACNT] = 1,0
	//		11 : Generate the EPWMxSOCA pulse on the third event: ETPS[SOCACNT] = 1,1
	EPwm1Regs.ETPS.bit.SOCAPRD = ET_1ST;        // Generate pulse on 1st event

}
*/

//===========================================================================
// End of file.
//===========================================================================
