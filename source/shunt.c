#include "shunt.h"
#include "variable.h"
#include "MPLD.h"


long Timer1start=0;
float Time_shunt = 0.;


interrupt void shuntA(void)

{
	IERBackupshunt = IER;
//	IER = M_INT1|M_INT9|M_INT2|M_INT12|M_INT3;		// Fault, CC int Enable 1080
	IER = M_INT1|M_INT12;			/* HW fault (XINT1,XINT2,XINT3,XINT4,XINT5), CC */
	EINT; 
	LED1_OFF;

	Timer1start = ReadCpuTimer2Counter();

	switch(Sector_num2)
	{
		case 1: // S0(000)->S1(100)->S2(110)->S7(111) : AB (+Ia,-Ic)
			//First 
			AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;
			while(ADC0_BUSY);
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
			ADCH[1] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD_COUT

			IA0_shunt = ((float) ADCH[1]-OffsetAin[1]) * ScaleAin[1];
//			IA0_shunt = ((float) ADCH[1]-Offsettemp) * ScaleAin[1];
			
			break;
		case 2: // S0(000)->S3(010)->S2(110)->S7(111) : BA (+Ib,-Ic)
			//Second
			AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;
			while(ADC0_BUSY);
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
			ADCH[1] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD_COUT

			IC0_shunt = -((float) ADCH[1]-OffsetAin[1]) * ScaleAin[1]; 
//			IC0_shunt = -((float) ADCH[1]-Offsettemp) * ScaleAin[1]; 
//	    	IA0_shunt = -IB0_shunt - IC0_shunt;
			break;
		case 3: // S0(000)->S3(010)->S4(011)->S7(111) : BC (+Ib,-Ia)
			//Third

			IC0_shunt = -IA0_shunt-IB0_shunt;
			break;
		case 4:	// S0(000)->S5(001)->S4(011)->S7(111) : CB (+Ic,-Ia)
			//Third

			IB0_shunt = -IA0_shunt-IC0_shunt;
			break; 
		case 5:	// S0(000)->S5(001)->S6(101)->S7(111) ; CA (+Ic,-Ib)
			//Second
			AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;
			while(ADC0_BUSY);
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
			ADCH[1] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD_COUT

			IB0_shunt = -((float) ADCH[1]-OffsetAin[1]) * ScaleAin[1];
//			IB0_shunt = -((float) ADCH[1]-Offsettemp) * ScaleAin[1];

//			IA0_shunt = -IB0_shunt - IC0_shunt;
			
			break;
		case 6:	// S0(000)->S1(100)->S6(101)->S7(111) ; AC (+Ia,-Ib)
			//First
			AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;
			while(ADC0_BUSY);
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
			ADCH[1] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD_COUT

			IA0_shunt = ((float) ADCH[1]-OffsetAin[1]) * ScaleAin[1];
//			IA0_shunt = ((float) ADCH[1]-Offsettemp) * ScaleAin[1];

			break;
	}


	Time_shunt = (float)(Timer1start - ReadCpuTimer2Counter())*SYSTEM_CLOCK_PRD;	/*	calculating CC loop time */

	EPwm4Regs.ETCLR.bit.INT = 1; 
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
	IER = IERBackupshunt;	
	LED1_ON;
	shuntA_num+=1;
	shunt_num+=1;
}

interrupt void shuntB(void)

{
	IERBackupshunt = IER;
//	IER = M_INT1|M_INT9|M_INT2|M_INT12|M_INT3;		// Fault, CC int Enable 1080
	IER = M_INT1|M_INT12;			/* HW fault (XINT1,XINT2,XINT3,XINT4,XINT5), CC */
	EINT; 
//	LED1_OFF;


	switch(Sector_num2)
	{
		case 1: // S0(000)->S1(100)->S2(110)->S7(111) : AB (+Ia,-Ic)
			//Second
			AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;
			while(ADC0_BUSY);
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
			ADCH[1] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD_COUT

			IC0_shunt = - ((float) ADCH[1]-OffsetAin[1]) * ScaleAin[1];
//			IC0_shunt = - ((float) ADCH[1]-Offsettemp) * ScaleAin[1];

//		    IB0_shunt = -IA0_shunt - IC0_shunt;  
		
			break;
		case 2: // S0(000)->S3(010)->S2(110)->S7(111) : BA (+Ib,-Ic)
			//First
			AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;
			while(ADC0_BUSY);
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
			ADCH[1] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD_COUT

			IB0_shunt = ((float) ADCH[1]-OffsetAin[1]) * ScaleAin[1];
		//	IB0_shunt = ((float) ADCH[1]-Offsettemp) * ScaleAin[1];
			
			break;
		case 3: // S0(000)->S3(010)->S4(011)->S7(111) : BC (+Ib,-Ia)
			//First
			AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;
			while(ADC0_BUSY);
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
			ADCH[1] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD_COUT

   			IB0_shunt = ((float) ADCH[1]-OffsetAin[1]) * ScaleAin[1];
		//	IB0_shunt = ((float) ADCH[1]-Offsettemp) * ScaleAin[1];
			
			break;
		case 4:	// S0(000)->S5(001)->S4(011)->S7(111) : CB (+Ic,-Ia)
			//Second
			AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;
			while(ADC0_BUSY);
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
			ADCH[1] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD_COUT

			IA0_shunt = - ((float) ADCH[1]-OffsetAin[1]) * ScaleAin[1];
		//	IA0_shunt = - ((float) ADCH[1]-Offsettemp) * ScaleAin[1];
			
		//	IB0_shunt = -IA0_shunt - IC0_shunt;  

			break; 
		case 5:	// S0(000)->S5(001)->S6(101)->S7(111) ; CA (+Ic,-Ib)
			//Third			
			IA0_shunt = -IB0_shunt - IC0_shunt;  

			
			break;
		case 6:	// S0(000)->S1(100)->S6(101)->S7(111) ; AC (+Ia,-Ib)
			//Third
			IC0_shunt = -IA0_shunt - IB0_shunt;  

			
			break;
	}



	EPwm5Regs.ETCLR.bit.INT = 1; 
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
	IER = IERBackupshunt;	
//	LED1_ON;
	shuntB_num+=1;
	shunt_num+=1;
}

interrupt void shuntC(void)

{
	IERBackupshunt = IER;
//	IER = M_INT1|M_INT9|M_INT2|M_INT12|M_INT3;		// Fault, CC int Enable 1080
	IER = M_INT1|M_INT12;			/* HW fault (XINT1,XINT2,XINT3,XINT4,XINT5), CC */
	EINT; 
//	LED1_OFF;



	switch(Sector_num2)
	{
		case 1: // S0(000)->S1(100)->S2(110)->S7(111) : AB (+Ia,-Ic)
			//Third				
			IB0_shunt = -IA0_shunt - IC0_shunt;  
 
			
			break;
		case 2: // S0(000)->S3(010)->S2(110)->S7(111) : BA (+Ib,-Ic)
			//Third			
			IA0_shunt = -IB0_shunt - IC0_shunt;  

			
			break;
		case 3: // S0(000)->S3(010)->S4(011)->S7(111) : BC (+Ib,-Ia)
			//Second
			AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;

			while(ADC0_BUSY);	
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
			ADCH[1] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD_COUT

			IA0_shunt = -((float) ADCH[1]-OffsetAin[1]) * ScaleAin[1];
		//	IA0_shunt = -((float) ADCH[1]-Offsettemp) * ScaleAin[1];
		
		//	IC0_shunt = -IA0_shunt - IB0_shunt;  
			
			
			break;
		case 4:	// S0(000)->S5(001)->S4(011)->S7(111) : CB (+Ic,-Ia)
			//First
			AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;

			while(ADC0_BUSY);	
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
			ADCH[1] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD_COUT

			IC0_shunt = ((float) ADCH[1]-OffsetAin[1]) * ScaleAin[1];				
	//		IC0_shunt = ((float) ADCH[1]-Offsettemp) * ScaleAin[1];				
			
			break; 
		case 5:	// S0(000)->S5(001)->S6(101)->S7(111) ; CA (+Ic,-Ib)
			//First
		 	AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;

			while(ADC0_BUSY);	
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
			ADCH[1] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD_COUT

  			IC0_shunt = ((float) ADCH[1]-OffsetAin[1]) * ScaleAin[1];				
		//	IC0_shunt = ((float) ADCH[1]-Offsettemp) * ScaleAin[1];				
			
			break;
		case 6:	// S0(000)->S1(100)->S6(101)->S7(111) ; AC (+Ia,-Ib)
			//Second
			AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;

			while(ADC0_BUSY);	
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
			ADCH[1] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD_COUT

			IB0_shunt = -((float) ADCH[1]-OffsetAin[1]) * ScaleAin[1];
		//	IB0_shunt = -((float) ADCH[1]-Offsettemp) * ScaleAin[1];
			
		//	IC0_shunt = -IA0_shunt - IB0_shunt;  
		
			
			break;
	}


	EPwm6Regs.ETCLR.bit.INT = 1; 
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
	IER = IERBackupshunt;	
//	LED1_ON;
	shuntC_num+=1;
	shunt_num+=1;

}




interrupt void shuntU(void)

{
	IERBackupshunt = IER;
//	IER = M_INT1|M_INT9|M_INT2|M_INT12|M_INT3;		// Fault, CC int Enable 1080
	IER = M_INT1|M_INT12;			/* HW fault (XINT1,XINT2,XINT3,XINT4,XINT5), CC */
	EINT; 
//	LED0_OFF;

	Timer1start = ReadCpuTimer2Counter();

		AD_RD0 = 0x500;
	
		delaycc(0.005e-6);

    	ADC0_SOC_START;
		delaycc(0.005e-6);

		ADC0_SOC_END;
		
		while(ADC0_BUSY);
		
		ADCH[6] = (AD_RD0 - 0x0800) & 0x0FFF;			//		Vvn
		ADCH[7] = (AD_RD0 - 0x0800) & 0x0FFF;			//		Dummy

		IU0_shunt = -((float) ADCH[6]-OffsetAin[6]) * ScaleAin[6];



	switch(Sector_num1)
	{
		case 1: // S0(000)->S1(100)->S2(110)->S7(111) : AB (+Ia,-Ic)
			//First 
			AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;
			while(ADC0_BUSY);
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
			ADCH[1] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD_COUT

			IU0_shunt = ((float) ADCH[0]-OffsetAin[0]) * ScaleAin[0];
			break;
		case 2: // S0(000)->S3(010)->S2(110)->S7(111) : BA (+Ib,-Ic)
			//Second
			AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;
			while(ADC0_BUSY);
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
			ADCH[1] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD_COUT

			IW0_shunt = -((float) ADCH[0]-OffsetAin[0]) * ScaleAin[0]; 
			IU0_shunt = -IV0_shunt - IW0_shunt;
			
			break;
		case 3: // S0(000)->S3(010)->S4(011)->S7(111) : BC (+Ib,-Ia)
			//Third
			IW0_shunt = -IU0_shunt-IV0_shunt;
			
			break;
		case 4:	// S0(000)->S5(001)->S4(011)->S7(111) : CB (+Ic,-Ia)
			//Third
			IV0_shunt = -IU0_shunt-IW0_shunt;
			break; 
		case 5:	// S0(000)->S5(001)->S6(101)->S7(111) ; CA (+Ic,-Ib)
			//Second
			AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;
			while(ADC0_BUSY);
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
			ADCH[1] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD_COUT

			IV0_shunt = -((float) ADCH[0]-OffsetAin[0]) * ScaleAin[0];
			IU0_shunt = -IV0_shunt - IW0_shunt;
			
			break;
		case 6:	// S0(000)->S1(100)->S6(101)->S7(111) ; AC (+Ia,-Ib)
			//First
			AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;
			while(ADC0_BUSY);
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
			ADCH[1] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD_COUT

			IU0_shunt = ((float) ADCH[0]-OffsetAin[0]) * ScaleAin[0];
			break;
	}


	Time_shunt = (float)(Timer1start - ReadCpuTimer2Counter())*SYSTEM_CLOCK_PRD;	/*	calculating CC loop time */

	EPwm1Regs.ETCLR.bit.INT = 1; 
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
	IER = IERBackupshunt;	
//	LED0_ON;
	shuntU_num+=1;
	shunt_num+=1;
}



interrupt void shuntV(void)

{
	IERBackupshunt = IER;
//	IER = M_INT1|M_INT9|M_INT2|M_INT12|M_INT3;		// Fault, CC int Enable 1080
	IER = M_INT1|M_INT12;			/* HW fault (XINT1,XINT2,XINT3,XINT4,XINT5), CC */
	EINT; 
//	LED1_OFF;

	
		AD_RD0 = 0x500;
	
		delaycc(0.005e-6);

    	ADC0_SOC_START;
		delaycc(0.005e-6);

		ADC0_SOC_END;
		
		while(ADC0_BUSY);
		
		ADCH[6] = (AD_RD0 - 0x0800) & 0x0FFF;			//		Vvn
		ADCH[7] = (AD_RD0 - 0x0800) & 0x0FFF;			//		Dummy

		IV0_shunt = -((float) ADCH[7]-OffsetAin[7]) * ScaleAin[7];






	switch(Sector_num1)
	{
		case 1: // S0(000)->S1(100)->S2(110)->S7(111) : AB (+Ia,-Ic)
			//Second
			AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;
			while(ADC0_BUSY);
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
			IW0_shunt = - ((float) ADCH[0]-OffsetAin[0]) * ScaleAin[0];
			IV0_shunt = -IU0_shunt - IW0_shunt;  
			break;
		case 2: // S0(000)->S3(010)->S2(110)->S7(111) : BA (+Ib,-Ic)
			//First

			AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;
			while(ADC0_BUSY);
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
			IV0_shunt = ((float) ADCH[0]-OffsetAin[0]) * ScaleAin[0];
			break;
		case 3: // S0(000)->S3(010)->S4(011)->S7(111) : BC (+Ib,-Ia)
			//First

			AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;
			while(ADC0_BUSY);
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
   			IV0_shunt = ((float) ADCH[0]-OffsetAin[0]) * ScaleAin[0];
			break;
		case 4:	// S0(000)->S5(001)->S4(011)->S7(111) : CB (+Ic,-Ia)
			//Second

			AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;
			while(ADC0_BUSY);
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
			IU0_shunt = - ((float) ADCH[0]-OffsetAin[0]) * ScaleAin[0];
			IV0_shunt = -IU0_shunt - IW0_shunt;  
			break; 
		case 5:	// S0(000)->S5(001)->S6(101)->S7(111) ; CA (+Ic,-Ib)
			//Third			
			IU0_shunt = -IV0_shunt - IW0_shunt;  
			break;
		case 6:	// S0(000)->S1(100)->S6(101)->S7(111) ; AC (+Ia,-Ib)
			//Third
			IW0_shunt = -IU0_shunt - IV0_shunt;  
			break;
	}



	EPwm2Regs.ETCLR.bit.INT = 1; 
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
	IER = IERBackupshunt;	
//	LED1_ON;
	shuntV_num+=1;
	shunt_num+=1;
}


interrupt void shuntW(void)

{
	IERBackupshunt = IER;
//	IER = M_INT1|M_INT9|M_INT2|M_INT12|M_INT3;		// Fault, CC int Enable 1080
	IER = M_INT1|M_INT12;			/* HW fault (XINT1,XINT2,XINT3,XINT4,XINT5), CC */
	EINT; 
//	LED1_OFF;

	 
		AD_RD1 = 0x500;

		delaycc(0.005e-6);

    	ADC1_SOC_START;
	
		delaycc(0.005e-6);
	
		ADC1_SOC_END;
	
		while(ADC1_BUSY);

		ADCH[8] = (AD_RD1 - 0x0800) & 0x0FFF;			//		Vwn
		ADCH[9] = (AD_RD1 - 0x0800) & 0x0FFF;			//		Dummy

		IW0_shunt = -((float) ADCH[9]-OffsetAin[9]) * ScaleAin[9];



		

	switch(Sector_num1)
	{
		case 1: // S0(000)->S1(100)->S2(110)->S7(111) : AB (+Ia,-Ic)
			//Third				
			IV0_shunt = -IU0_shunt - IW0_shunt;  
			break;
		case 2: // S0(000)->S3(010)->S2(110)->S7(111) : BA (+Ib,-Ic)
			//Third			
			IU0_shunt = -IV0_shunt - IW0_shunt;  
			break;
		case 3: // S0(000)->S3(010)->S4(011)->S7(111) : BC (+Ib,-Ia)
			//Second
			AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;

			while(ADC0_BUSY);	
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
			ADCH[1] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD_COUT
			IU0_shunt = -((float) ADCH[0]-OffsetAin[0]) * ScaleAin[0];
			IW0_shunt = -IU0_shunt - IV0_shunt;  
			break;
		case 4:	// S0(000)->S5(001)->S4(011)->S7(111) : CB (+Ic,-Ia)
			//First
			AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;

			while(ADC0_BUSY);	
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
			ADCH[1] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD_COUT

			IW0_shunt = ((float) ADCH[0]-OffsetAin[0]) * ScaleAin[0];				
			break; 
		case 5:	// S0(000)->S5(001)->S6(101)->S7(111) ; CA (+Ic,-Ib)
			//First
  			AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;
	
			while(ADC0_BUSY);	
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
			ADCH[1] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD_COUT
  			IW0_shunt = ((float) ADCH[0]-OffsetAin[0]) * ScaleAin[0];				
			break;
		case 6:	// S0(000)->S1(100)->S6(101)->S7(111) ; AC (+Ia,-Ib)
			//Second
			AD_RD0= 0x100;
			delaycc(0.005e-6);
			ADC0_SOC_START;
			delaycc(0.005e-6);
			ADC0_SOC_END;

			while(ADC0_BUSY);	
			ADCH[0] = (AD_RD0 - 0x0800) & 0x0FFF;
			ADCH[1] = (AD_RD0 - 0x0800) & 0x0FFF;			//		AD_COUT

			IV0_shunt = -((float) ADCH[0]-OffsetAin[0]) * ScaleAin[0];
			IW0_shunt = -IU0_shunt - IV0_shunt;  
			break;
	}


	EPwm3Regs.ETCLR.bit.INT = 1; 
	PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
	IER = IERBackupshunt;	
//	LED1_ON;
	shuntW_num+=1;
	shunt_num+=1;

}
